# MCE API Documentation

This documentation is built with [Slate](https://github.com/slatedocs/slate).

You just have to write the content in markdown, and it generates a nice HTML documentation that you can access on [api-documentation.mce.fr](https://github.com/slatedocs/slate).

# How it works

1. You write your documentation in **markdown**
2. You push it to `master` branch and launch a deployment script
3. It generates a static HTML site and pushes it to `gh-pages` branch

Our server automatically pull the `gh-pages` branch, so as soon you launch the deployment script, it pushes a new commit on `gh-pages`, and [api-documentation.mce.fr](http://api-documentation.mce.fr) is automatically updated.

## Installation

To install it on your local environment, please see [these instructions](https://github.com/slatedocs/slate/wiki#getting-started).

## Usage

1. You can run a local server to view what you type: `bundle exec middleman server`
2. You write the documentation in markdown files located in `source/includes`
3. Once you did your changes, you push it to `master`
4. Run: `./deploy.sh`
