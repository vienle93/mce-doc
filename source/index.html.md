---
title: MCE API

language_tabs:
  - javascript

includes:
  - auth.md

search: true
---

# Introduction

Welcome to the MCE API documentation!

> The baseURL of prod.

```javascript
axios.defaults.baseURL = 'https://maconsultationesthetique.com/api/v1'
```

> The baseURL of preprod.

```javascript
axios.defaults.baseURL = 'https://preprod-admin.maconsultationesthetique.com/api/v1'
```

> The baseURL of local.

```javascript
axios.defaults.baseURL = 'http://admin.mce.local/api/v1'
```

## Get a CSRF cookie

```javascript
axios.get('/rest/session/token').then(response => {
  // ...
})
```

> This request returns a 200 response.

Returns an HTTP response that sets a cookie containing the CSRF token.

This cookie is necessary in order to be able to make `GET`, `POST`, `PUT` and `DELETE` calls to the API. If this cookie is not sent, then a 403 response will be returned (_"X-CSRF-Token request header is missing"_).

### HTTP request

`GET /rest/session/token`

### Parameters

None.

### Responses

`200 - String`

## Set Basic token and CSRF cookie

```javascript
axios.defaults.withCredentials = true;
axios.defaults.headers.common['Accept'] = 'application/json';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['Authorization'] = 'Basic cmVzdF91c2VyOjNxUjYhN3t6Vit4Wg==';
axios.defaults.headers.common['X-CSRF-Token'] = 'csrf_token';
```
